import torch

from pynllb import Wrapper
from pathlib import Path
import pytest


@pytest.fixture
def base_path() -> Path:
    return Path(__file__).parent.parent.parent


def test_autodetect(base_path: Path):
    sentence = "Secondo quanto ricostruito dagli uomini della Polizia Cantonale intervenuti sul posto, il conducente " \
               "della Golf stava percorrendo la strada principale quando all'altezza di un incrocio e svoltando a " \
               "sinistra per immettersi in una strada secondaria si è scontrato con il bolide proveniente dalla " \
               "direzione opposta."
    translation = "According to reconstructions carried out by the local Cantonal Police, the driver of the Golf was " \
                  "driving on the main road when, at the height of a crossroads and turning left to enter a secondary " \
                  "road, he collided with the car coming from the opposite direction."
    wrapper = Wrapper(spm_model=base_path / 'resources' / 'flores-200-sacrebleu-spm.model',
                      nllb_model=base_path / 'resources' / 'nllb-200-distilled-dense-600m.model',
                      nllb_dictionary=base_path / 'resources' / 'nllb-200.dictionary',
                      flores_langs=base_path / 'resources' / 'flores-200-langs.txt')
    assert wrapper.translate(sentence, "ita_Latn", "eng_Latn") == translation


def test_cpu(base_path: Path):
    if torch.cuda.is_available():
        sentence = "Secondo quanto ricostruito dagli uomini della Polizia Cantonale intervenuti sul posto, il conducente " \
                   "della Golf stava percorrendo la strada principale quando all'altezza di un incrocio e svoltando a " \
                   "sinistra per immettersi in una strada secondaria si è scontrato con il bolide proveniente dalla " \
                   "direzione opposta."
        translation = "According to reconstructions carried out by the local Cantonal Police, the driver of the Golf was " \
                      "driving on the main road when, at the height of a crossroads and turning left to enter a secondary " \
                      "road, he collided with the car coming from the opposite direction."
        wrapper = Wrapper(spm_model=base_path / 'resources' / 'flores-200-sacrebleu-spm.model',
                          nllb_model=base_path / 'resources' / 'nllb-200-distilled-dense-600m.model',
                          nllb_dictionary=base_path / 'resources' / 'nllb-200.dictionary',
                          flores_langs=base_path / 'resources' / 'flores-200-langs.txt',
                          cpu=True)
        assert wrapper.translate(sentence, "ita_Latn", "eng_Latn") == translation
    else:
        print("Ignored")
