#!/bin/bash
set -e
set -u

SCRIPT_DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
IMAGE_NAME="registry.gitlab.com/fcavalieri/pynllb/ci"
IMAGE_VERSION="0.0.3"
IMAGE_TAG="${IMAGE_NAME}:${IMAGE_VERSION}"

echo "Creating and publishing docker image $IMAGE_TAG"
if docker manifest inspect $IMAGE_TAG >/dev/null; then
    echo "Image $IMAGE_TAG already exists!!!"
    exit 1
else
    echo "Image $IMAGE_TAG does not exists!"
fi

(
  cd "$SCRIPT_DIR"
  docker build --no-cache -t ${IMAGE_TAG}  .
  #docker build -t ${IMAGE_TAG}  .
  docker push ${IMAGE_TAG}
)
