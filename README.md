# pynllb


## Downloading resources

Mandatory
```
wget -O resources/nllb-200.dictionary https://tinyurl.com/nllb200dictionary
wget -O resources/flores-200-sacrebleu-spm.model https://tinyurl.com/flores200sacrebleuspm
wget -O resources/flores-200-langs.txt https://raw.githubusercontent.com/facebookresearch/fairseq/nllb/examples/nllb/modeling/scripts/flores200/langs.txt
```

One of:
```
cd resources
wget -O resources/nllb-200-distilled-dense-600m.model https://tinyurl.com/nllb200densedst600mcheckpoint
wget -O resources/nllb-200-distilled-dense-1b.model https://tinyurl.com/nllb200densedst1bcheckpoint
wget -O resources/nllb-200-dense-1b.model https://tinyurl.com/nllb200dense1bcheckpoint
wget -O resources/nllb-200-dense-3b.model https://tinyurl.com/nllb200dense3bcheckpoint
```

Not currently supported:
```
wget -O resources/nllb-200-moe-54b.model.tar.gz https://tinyurl.com/nllb200moe54bmodel
```


